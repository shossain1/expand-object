using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using Slider = UnityEngine.UI.Slider;

public class ExpandObject : MonoBehaviour
{
    private Coroutine coroutine;
    [SerializeField] private List<Transform> childObjects;
    [SerializeField] private List<Vector3> childObjectsInitPosition;
    [SerializeField] private List<Vector3> directionList;
    private Vector3 direction = Vector3.zero;

    public Slider expandSlider;
    // Start is called before the first frame update
    void Start()
    {
        foreach (Transform child in transform)
        {
            childObjects.Add(child);
            childObjectsInitPosition.Add(child.position);
            direction = (RandomOnSphere(child.position, 5f) - child.position);
            directionList.Add(direction);
        }
    }

    public void ExpandToggle(bool isOn)
    {
        if (isOn)
        {
            expandSlider.gameObject.SetActive(isOn);
            coroutine = StartCoroutine(Expand());
        }
        else
        {
            expandSlider.gameObject.SetActive(isOn);
            StopCoroutine(coroutine);
            //StopAllCoroutines();
        }
        
    }
    
    private IEnumerator Expand()
    {
        // childObjects[0].position = Vector3.Lerp(new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f), Time.time/2);
        /*childObjects[1].position = Vector3.Lerp(new Vector3(0f, 0f, 0f), new Vector3(2f, 0f, 0f), Time.time/2);
        childObjects[2].position = Vector3.Lerp(new Vector3(0f, 0f, 0f), new Vector3(-2f, 0f, 0f), Time.time/2);
        childObjects[3].position = Vector3.Lerp(new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 2f), Time.time/2);
        childObjects[4].position = Vector3.Lerp(new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, -2f), Time.time/2);*/
        while (true)
        {
            for (int i = 0; i < childObjects.Count; i++)
            {
                childObjects[i].position = Vector3.Lerp(childObjectsInitPosition[i], directionList[i], expandSlider.value);
            }
            yield return null;
        }
        
        /*childObjects[1].position = Vector3.Lerp(new Vector3(0f, 0f, 0f), new Vector3(2f, 0f, 0f), expandSlider.value);
        childObjects[2].position = Vector3.Lerp(new Vector3(0f, 0f, 0f), new Vector3(-2f, 0f, 0f), expandSlider.value);
        childObjects[3].position = Vector3.Lerp(new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 2f), expandSlider.value);
        childObjects[4].position = Vector3.Lerp(new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, -2f), expandSlider.value);*/
    }
    
    private Vector3 RandomOnSphere(Vector3 center, float radius)
    {
        return center + Random.onUnitSphere * radius;
    }
}
